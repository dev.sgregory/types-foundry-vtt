
declare interface RegisterSheetOption {
  types : string[];
  makeDefault : boolean;
}

declare class Actors extends Collection {
  get object() : Actor;

  static socketListeners(socket : SocketIO.Socket)  : void;

  static registerSheet<T extends Application>(
    scope : string,
    sheetClass?: new() => T,
    { types, makeDefault}? : { types? : any[], makeDefault? : boolean}) : void;

	static unregisterSheet<T extends Application>(
    scope: string,
    sheetClass?: new() => T,
    { types }?: { types: any[] }) : void;
}
