declare const CONFIG: {
	Actor: {
		entityClass: typeof Entity | null,
		collection: typeof Collection | null,
		sheetClasses: object,
		sidebarIcon: string
	},

	Item: {
		entityClass: typeof Entity | null,
		collection: typeof Collection | null,
		sheetClass: typeof BaseEntitySheet | null,
		sheetClasses: object,
		sidebarIcon: string
	},

	JournalEntry: {
		entityClass: typeof Entity | null,
		sheetClass: typeof BaseEntitySheet | null,
		noteIcons: { [key: string]: string },
		sidbarIcon: string
	},

	Scene: {
		entityClass: any | null,
		sheetClass: typeof BaseEntitySheet | null,
		notesClass: any | null,
		sidebarIcon: string
	},

	Playlist: {
		entityClass: typeof Entity | null,
		sheetClass: typeof BaseEntitySheet | null,
		sidebarIcon: string
	},

	SightLayer: {
		debug: boolean;
	},

	controlIcons: {
		combat: string,
		visiblity: string,
		effects: string,
		lock: string,
		up: string,
		down: string,
		defeated: string
	},

	fontFamilies: string[],
	defaultFontFamily: string,

	weatherEffects: string[],
	statusEffects: string[],

	sounds: {
		dice: string,
		lock: string,
		notification: string,
		combat: string
	},

	supportedLanguages: {
		en: string,
		br: string,
		fr: string,
		nl: string,
		it: string
	},

	maxCanvasZoom: number
}