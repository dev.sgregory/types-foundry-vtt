declare function loadTemplates(paths: string[]): Promise<void>;
declare function renderTemplate(path : string, data? : object) : Promise<HTMLElement>;
declare function getProperty(obj : object, key : string) : any;
declare function duplicate(original : object) : any;
declare function mergeObject(
  orignal : object,
  other : object,
  insert? : boolean,
  overwrite? : boolean,
  inplace? : boolean) : object;

declare let canvas : Canvas;
declare let game : Game;


declare interface Math {
    clamped(x : number, min : number, max : number) : number;
}

declare let DEFAULT_TOKEN : string;
