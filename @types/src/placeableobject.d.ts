declare class PlaceableObject extends PIXI.Container {
	data: object;
	scene: Scene;
	fov: PIXI.Polygon;
	controlIcon: ControlIcon;

	readonly layer: PlaceablesLayer;
	readonly id: number;
	readonly coords: number[];
	readonly center: { x: number, y: number };
	readonly owner: boolean;
	readonly sheet: Application;
  public actor : Actor;

	static create(sceneId: string, data: object, options: { displaySheet: boolean }): Promise<PlaceableObject>;

	constructor(data: object, scene: Scene);

	clear(): void;
	control({ multiSelect, releaseOthers }: { multiSelect: boolean, releaseOthers: boolean }): boolean;
	release(): boolean;
	draw(): void;
	refresh(): void;
	sortToFront(): void;
	sortToBack(): void;
	displayToFront(): void;
	clone(): PlaceableObject;
	rotate(angle: number, snap: boolean): void;
	update(sceneId: string, data: object, options: object): Promise<any>;
	delete(sceneId: string, options: object): Promise<any>;
}
