declare class Items extends Collection {
	readonly object: Item;

	static registerSheet<T extends Application>(
    scope: string,
    sheetClass?: new() => T,
    { types, makeDefault }?: { types?: any[], makeDefault?: boolean }): void;
  
	static unregisterSheet<T extends Application>(scope: string, sheetClass?: new() => T, { types }?: { types: any[] }): void;
}
