declare class Tabs {

  constructor(tabs : HTMLElement | JQuery, {initial, callback} : { initial : string, callback? : Function });
}
