declare class Token extends PlaceableObject {
	actor: Actor;

	static readonly layer: PlaceablesLayer;

	readonly owner: boolean;
	readonly canViewSheet: boolean;
	readonly name: string;
	// @ts-ignore
	readonly sheet: TokenConfig;
	readonly w: number;
	readonly h: number;
	readonly inCombat: boolean;
	readonly isVisible: boolean;
	readonly emitsLight: boolean;
	readonly dimRadius: number;
	readonly dimLightRadius: number;
	readonly brightRadius: number;
	readonly brightLightRadius: number;

	static moveMany(offsets: number[], rotate: boolean): Promise<void>;

	constructor(data: object, scene: Scene);

	draw(): Promise<void>;
	refresh(): void;
	drawBars(): void;
	drawTooltip(): void;
	drawEffects(): void;
	animateMovement(ray: Ray): Promise<void>;
	checkCollision(destination: { x: number, y: number }): boolean;
	checkCollision(destination: { x: number, y: number }, { drag }: { drag: boolean }): boolean;
	control({ multiSelect, releaseOthers, initializeSight }: { multiSelect: boolean, releaseOthers: boolean, initializeSight: boolean }): boolean;
	// @ts-ignore -- TypeScript doesn't like the overloaded arguments here..
	release({ resetSight }: { resetSight: boolean }): boolean;
	getCenter(x: number, y: number): { x: number, y: number };
	setPosition(x: number, y: number): Promise<void>;
	toggleCombat(): Promise<Token>;
	toggleEffect(texture: string): Promise<void>;
	toggleOverlay(texture: string): Promise<void>;
	toggleVisibility(): Promise<void>;
	getSightOrigin(): { x: number, y: number };
	shiftPosition(dx: number, dy: number): Promise<void>;
	delete(sceneId: string, options: object): Promise<any>;
}
