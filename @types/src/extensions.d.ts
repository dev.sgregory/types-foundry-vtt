declare interface String {
	capitalize(): string;
	titleCase(): string;
	stripScripts(): string;
}
declare interface Number {
	ordinalString(): string;
	paddedString(digits: number): string;
	signedString(): string;
	between(a: number, b: number): boolean;
	between(a: number, b: number, inclusive: boolean): boolean;
}
declare interface NumberConstructor {
	isNumeric(n: any): boolean;
}