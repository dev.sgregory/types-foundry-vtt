declare class Entity {
	// Variables
	data: any;
	options: object;
	apps: object;

	// Properties
	static readonly entity: Entity;
	static readonly collection: Collection;

	readonly entity: Entity;
	readonly id: string;
	readonly name: string;
	readonly collection: Collection;
	readonly sheet: BaseEntitySheet;
	readonly permission: number;
	readonly owner: boolean;
	readonly visible: boolean;
	readonly limited: boolean;

	// Methods
	static create(data: object, options: { temporary?: boolean, displaySheet?: boolean }): Promise<Entity>;
	constructor(data: object, options: object);

	prepareData(data: object): object;
	render(...args: any): void;
	hasPerm(user: User, permission: number): boolean;
	hasPerm(user: User, permission: number, exact: boolean): boolean;
	update(data: object, options?: object): Promise<Entity>;
	delete(options: object): Promise<string>;
	setFlag(scope: string, key: string, value: any): Promise<Entity>;
	getFlag(scope: string, key: string): any;
	exportToJSON(): void;
	importFromJSON(json: string): Promise<Entity>;
	importDialog(): Promise<void>;
}
