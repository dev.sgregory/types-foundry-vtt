type FormApplicationOptions = {
	height: number, width: number,
	top: number, left: number,
	popOut: boolean,
	minimizable: boolean,
	resizable: boolean,
	scale: number ,

	template: string,
	id: string,
	classes: string[],
	title: string,

	closeOnSubmit: boolean,
	submitOnClose: boolean,
	submitOnUnfocus: boolean,
	editable: boolean
}

declare class FormApplication extends Application {
	object: any;
	form: HTMLElement;
	filePickers: FilePicker[];
	editors: object[];

	static readonly defaultOptions: FormApplicationOptions;

	readonly isEditable: boolean;

	constructor(object: any, options: FormApplicationOptions);

	getData(): { [key: string]: any };
}