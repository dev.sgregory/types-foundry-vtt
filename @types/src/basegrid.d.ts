/// <reference types="pixi.js" />

declare class BaseGrid extends PIXI.Container {
	options: object;
	w: number;
	h: number;
	highlight: PIXI.Container;

	constructor(options: object);

	draw(): BaseGrid;
	highlightGridPOsition(layer: object, options: object): boolean;
	getTopLeft(x: number, y: number): number[];
	getCenter(x: number, y: number): number[];
	getSnappedPosition(x: number, y: number): { x: number, y: number };
	getSnappedPosition(x: number, y: number, interval: number): { x: number, y: number };
	getGridPositionFromPixels(x: number, y: number): number[];
	getPixelsFromGridPOsition(x: number, y: number): number[];
	shiftPosition(x: number, y: number, dx: number, dy: number): number[];
	measureDistance(p0: { x: number, y: number }, p1: { x: number, y: number }): number;
	getNeighbors(row: number, col: number): object[];
}