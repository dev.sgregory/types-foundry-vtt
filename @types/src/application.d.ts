/// <reference types="jquery" />

type ApplicationOptions = {
	height: number, width: number,
	top: number, left: number,
	popOut: boolean,
	minimizable: boolean,
	resizable: boolean,
	scale: number ,

	template: string,
	id: string,
	classes: string[],
	title: string
};

declare class Application {
	options: ApplicationOptions;
	appId: number;
	position: { width: number, height: number, left: number, top: number, scale: number };

	static readonly defaultOptions: ApplicationOptions;

	readonly id: string;
	readonly element: JQuery | HTMLElement;
	readonly template: string;
	readonly popOut: boolean;
	readonly title: string;

	constructor(options: ApplicationOptions);

	getData(options: object): object | Promise<object>;
	render(force: boolean, options?: { left: number, top: number, width: number, height: number, scale: number, log: boolean, renderContext: string, renderData: any }): void;
	activateListeners(html: JQuery|HTMLElement): void;
	close(): Promise<void>;
	minimize(): Promise<void>;
	maximize(): Promise<void>;
	setPosition({left, top, width, height, scale}: { left: number, top: number, width: number, height: number, scale: number }): void;
}
